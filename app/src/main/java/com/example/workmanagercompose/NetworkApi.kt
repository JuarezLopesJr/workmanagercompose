package com.example.workmanagercompose

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.GET

interface NetworkApi {

    @GET("/endpointAPI to get image")
    suspend fun downloadImage(): Response<ResponseBody>

    companion object {
        val instance by lazy {
            Retrofit.Builder()
                .baseUrl("base endpointAPI")
                .build()
                .create(NetworkApi::class.java)
        }
    }
}